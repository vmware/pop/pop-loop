.. pop-loop documentation master file.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. _README:

.. include:: ../README.rst

.. toctree::
   :caption: Get Involved
   :maxdepth: 2
   :glob:
   :hidden:

   topics/contributing
   topics/license
   Project Repository <https://gitlab.com/vmware/pop/pop-loop/>

.. toctree::
   :caption: Links
   :hidden:

   POP Project Source on GitLab <https://gitlab.com/vmware/pop>
   Idem Project Docs Portal <https://docs.idemproject.io>
   Idem Project Website <https://www.idemproject.io>
   Idem Project Source on GitLab <https://gitlab.com/vmware/idem>
