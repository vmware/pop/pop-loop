import asyncio

import pop.hub
import pytest


@pytest.fixture(scope="function", name="fub")
def f_hub():
    h = pop.hub.Hub()
    yield h


def test_loop_create(fub):
    fub.pop.loop.create(loop_plugin="selector")
    assert fub.pop.loop.CURRENT_LOOP == fub.pop.Loop


def test_loop_start(fub):
    fub.pop.loop.start(loop_plugin="selector")
    assert fub.pop.loop.CURRENT_LOOP == fub.pop.Loop


async def func(fub):
    async def _():
        ...

    coros = [_()]
    await asyncio.gather(*coros, return_exceptions=False)


def test_heist_salt_21(fub):
    fub.pop.loop.start(func(fub), loop_plugin="selector")
